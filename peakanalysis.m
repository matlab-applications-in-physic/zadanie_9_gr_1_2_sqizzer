%Huge file analysis
%�ukasz Giemza

fileID = fopen('Co60_1350V_x20_p7.dat', 'r'); %open the file to read from
histogram = zeros(1, 255); 
data = fread(fileID, 1E7, 'uint8'); %reads part of the given file (1E7 bits) we have to create a loop where the data is read part by prat
a = 1;


while a > 0 
    for i = 1:255;
        histogram(1, i) = histogram(1, i) + sum(data(:) == i); %count how many times 'i' occured in 'data' and save that in 'histogram'    %this part was taken fron Benajmin Str�czek   https://gitlab.com/matlab-applications-in-physic/zadanie_9_gr_1_2_benistr048/blob/master/zad_9  
    data = fread(fileID, 1E7, 'uint8'); %read the data (it is in the loop so parts are read every time the loop repeats
    [a, c] = size(data); %check the size of data to get the value of a
    end
end

fclose(fileID); % close the file


saveas(plot(histogram), 'Co60_1350V_x20_p7_histogram.pdf');
saveas(title('Histogram danych z przetwornika'), 'Co60_1350V_x20_p7_histogram.pdf');
saveas(xlabel('warto�ci'), 'Co60_1350V_x20_p7_histogram.pdf');
saveas(ylabel('Ilo�� wyst�pie�'), 'Co60_1350V_x20_p7_histogram.pdf');
%saves histogram to pdf


